# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
you can find some comments aroun Laravel crud here:
https://www.5balloons.info/tutorial-simple-crud-operations-in-laravel-5-5/

### How do I get set up? ###

* Summary
 - first install laravel using command: 
 composer create-project --prefer-dist laravel/laravel portabilis-php
 obs.: you must have composer installed on your machine
 
 - create a sql schema called portabilis-php
 - edit your .env file configuration located at your root project folder with the correct schema name, user and password
 - run php artisan make:model Student -a to create model with controller studens
 - run php artisan migrate to create tables on you database
 

### # Setting up the Routes ###

Add this following line to your web.php file under routes directory.


 - Route::resource('students', 'StudentController');
 



### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact