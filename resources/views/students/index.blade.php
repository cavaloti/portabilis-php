@extends('layout.layout')
@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">BIRTH</th>
            <th scope="col">CPF</th>
            <th scope="col">RG</th>
            <th scope="col">TEL</th>
            <th scope="col">Created At</th>
        </tr>
        </thead>
        <tbody>
        @foreach($students as $student)
            <tr>
                <th scope="row">{{$student->id}}</th>
                <td><a href="/students/{{$student->id}}">{{$student->name}}</a></td>
                <td>{{$student->birth}}</td>
                <td>{{$student->cpf}}</td>
                <td>{{$student->rg}}</td>
                <td>{{$student->telephone}}</td>
                <td>{{$student>created_at->toFormattedDateString()}}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="{{ URL::to('students/' . $student->id . '/edit') }}">
                            <button type="button" class="btn btn-warning">Edit</button>
                        </a>&nbsp;
                        <form action="{{url('students', [$student->id])}}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Delete"/>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
